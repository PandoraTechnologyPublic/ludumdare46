﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class BuildScript
{
    private static readonly bool IS_DEBUG_BUILD = true;
    
    [MenuItem("Builds/All platforms")]
    public static void BuildAllPlatforms()
    {
        BuildWindowsGame();
        BuildOsxGame();
        BuildLinuxGame();
    }

    [MenuItem("Builds/Windows Build")]
    public static void BuildWindowsGame()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        List<string> scenes = new List<string>();
        
        foreach (EditorBuildSettingsScene editorBuildSettingsScene in EditorBuildSettings.scenes)
        {
            scenes.Add(editorBuildSettingsScene.path);
        }

        buildPlayerOptions.locationPathName = Application.dataPath + "/../Versions/Windows/CurseOfUddh.exe";
        buildPlayerOptions.scenes = scenes.ToArray();
        buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        
        if (IS_DEBUG_BUILD)
        {
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer | BuildOptions.AllowDebugging;
        }

        BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
        
        // Debug.Log(buildReport.summary);
    }
    
    [MenuItem("Builds/OSX Build")]
    public static void BuildOsxGame()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        List<string> scenes = new List<string>();
        
        foreach (EditorBuildSettingsScene editorBuildSettingsScene in EditorBuildSettings.scenes)
        {
            scenes.Add(editorBuildSettingsScene.path);
        }

        buildPlayerOptions.locationPathName = Application.dataPath + "/../Versions/OSX/CurseOfUddh.app";
        buildPlayerOptions.scenes = scenes.ToArray();
        buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
        buildPlayerOptions.target = BuildTarget.StandaloneOSX;
        
        if (IS_DEBUG_BUILD)
        {
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer | BuildOptions.AllowDebugging;
        }

        BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
        
        // Debug.Log(buildReport.summary);
    }
    
    [MenuItem("Builds/Linux Build")]
    public static void BuildLinuxGame()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        List<string> scenes = new List<string>();
        
        foreach (EditorBuildSettingsScene editorBuildSettingsScene in EditorBuildSettings.scenes)
        {
            scenes.Add(editorBuildSettingsScene.path);
        }

        buildPlayerOptions.locationPathName = Application.dataPath + "/../Versions/Linux/CurseOfUddh.x86_64";
        buildPlayerOptions.scenes = scenes.ToArray();
        buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
        buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
        
        if (IS_DEBUG_BUILD)
        {
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer | BuildOptions.AllowDebugging;
        }

        BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
        
        // Debug.Log(buildReport.summary);
    }
}
