using Pandora.Managers;
using UnityEngine;

namespace Pandora.UI
{
    public class GameOverScreenLayout : MonoBehaviour
    {
        private GameManager _gameManager;
            
        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
        }
        
        private void Update()
        {
            if (_gameManager._playerInput.actions["UI/Submit"].ReadValue<float>() > 0)
            {
                OnNextButton();
            }
        }

        private void OnNextButton()
        {
            _gameManager.SendMessage(Message.ON_START_BUTTON);
        }
    }
}