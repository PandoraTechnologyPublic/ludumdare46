using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Pandora.UI
{
    public class DialogueUi : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private List<CharacterPortrait> _portraits;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Image _border;
        [SerializeField] private Image _fill;
        [SerializeField] private Color _playerBorderColor;
        [SerializeField] private Color _helplessBorderColor;
        [SerializeField] private float _fillSaturationMultiplier = 0.66f;

        [SerializeField] private List<AudioClip> _textClips;
        private AudioSource _audioSource;

        private CharacterPortrait _currentPortrait;
        private Coroutine _textDisplayCoroutine;

        private string _currentText;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _canvas.enabled = false;
        }

        private void OnEnable()
        {
            _text.SetText(_currentText);
        }

        public void Say(string text, Character speaker, Expression expression = Expression.NEUTRAL)
        {
            _currentText = text;
            _text.text = "";
            
            if (_textDisplayCoroutine != null)
            {
                StopCoroutine(_textDisplayCoroutine);
                _textDisplayCoroutine = null;
            }
            
            _textDisplayCoroutine = StartCoroutine(DisplayTextCoroutine(text, speaker));
            SetCharacter(speaker);
            _currentPortrait.SetExpression(expression);
            _canvas.enabled = true;
        }

        private IEnumerator DisplayTextCoroutine(string textToDisplay, Character speaker)
        {
            for (int currentCharacterIndex = 0; currentCharacterIndex < textToDisplay.Length; currentCharacterIndex++)
            {
                if (textToDisplay[currentCharacterIndex] != ' ')
                {
                    _audioSource.clip = _textClips[Random.Range(0, _textClips.Count)];
                    
                    if (speaker == Character.PLAYER)
                    {
                        _audioSource.pitch = 1 + Random.Range(-0.6f, -0.4f);
                    }
                    else
                    {
                        _audioSource.pitch = 1 + Random.Range(-0.2f, 0.2f);
                    }

                    _audioSource.Play();
                }

                _text.SetText(textToDisplay.Substring(0, currentCharacterIndex + 1));
                
                yield return new WaitForSeconds(0.03f);
            }
        }

        public void Hide()
        {
            _canvas.enabled = false;
        }

        private void SetCharacter(Character character)
        {
            if (_currentPortrait == null || _currentPortrait._id != character)
            {
                foreach (CharacterPortrait portrait in _portraits)
                {
                    if (portrait._id == character)
                    {
                        portrait.gameObject.SetActive(true);
                        _currentPortrait = portrait;
                    }
                    else
                    {
                        portrait.gameObject.SetActive(false);
                    }
                }

                if (character == Character.PLAYER)
                {
                    _border.color = _playerBorderColor;
                }
                else
                {
                    _border.color = _helplessBorderColor;
                }

                Color.RGBToHSV(_border.color, out float h, out float s, out float v);
                Color fillColor = Color.HSVToRGB(h, s * _fillSaturationMultiplier, v);
                _fill.color = fillColor;
            }
        }
    }
}
