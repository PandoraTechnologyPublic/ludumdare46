using System.Collections;
using Pandora.Managers;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class MainMenuScreenLayout : MonoBehaviour
    {
        public Button _playButton;
        public Button _creditButton;
        public Button _exitButton;

        private GameManager _gameManager;
            
        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            
            _playButton.onClick.AddListener(OnPlayButton);
            _creditButton.onClick.AddListener(OnCreditButton);
            _exitButton.onClick.AddListener(OnExitButton);
        }

        private void OnEnable()
        {
            StartCoroutine(ActivateButton(_playButton));     
        }

        private IEnumerator ActivateButton(Button playButton)
        {
            yield return null;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(playButton.gameObject);
        }

        private void OnPlayButton()
        {
            _gameManager.SendMessage(Message.ON_START_BUTTON);
        }

        private void OnCreditButton()
        {
            _gameManager.SendMessage(Message.ON_CREDIT_BUTTON);
        }

        private void OnExitButton()
        {
            Application.Quit();
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #endif
        }
    }
}