using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class ProgressBarLayout : MonoBehaviour
    {
        public Image _progressBarImage;

        public void SetProgress(float value, float maxValue)
        {
            _progressBarImage.fillAmount = value / maxValue;
        }
    }
}