using System;
using System.Collections.Generic;
using Pandora.Components;
using Pandora.Controllers;
using Pandora.GameScript.Actions;
using Pandora.GameState;
using Pandora.Managers;
using UnityEngine;

namespace Pandora.UI
{
    public class DialogBubbleScreenLayout : MonoBehaviour
    {
        public SpeechBubble _speechBubblePrefab;
        public RectTransform _speechBubbleLayer;

        private readonly Dictionary<SpeakerComponent, SpeechBubble> _speechBubbles = new Dictionary<SpeakerComponent, SpeechBubble>();
        private LevelManager _levelManager;
        private GameManager _gameManager;
        //
        private void Awake()
        {
            _levelManager = FindObjectOfType<LevelManager>();
            _gameManager = FindObjectOfType<GameManager>();
        }

        private void Update()
        {
            bool doesShowBubbles = true;

            if (_gameManager != null)
            {
                doesShowBubbles = _gameManager.IsCurrentState<IngameGameState>();
            }
            
            foreach (KeyValuePair<SpeakerComponent, SpeechBubble> speechBubble in _speechBubbles)
            {
                speechBubble.Value.gameObject.SetActive(doesShowBubbles);
                
                if (doesShowBubbles)
                {
                    UpdatePosition(speechBubble.Value, speechBubble.Key);
                }
            }
        }

        public void Say(SpeakerComponent speaker, string text)
        {
            if (!_speechBubbles.ContainsKey(speaker))
            {
                _speechBubbles.Add(speaker, GameObject.Instantiate(_speechBubblePrefab, _speechBubbleLayer));
            }

            SpeechBubble speechBubble = _speechBubbles[speaker];
            UpdatePosition(speechBubble, speaker);
            speechBubble.Say(text);
            speechBubble.gameObject.SetActive(true);
        }

        private void UpdatePosition(SpeechBubble speechBubble, SpeakerComponent speaker)
        {
            Vector3 worldToScreenPoint = _levelManager._ingameCamera.WorldToScreenPoint(speaker.transform.position + Vector3.up * 3);

            worldToScreenPoint.z = 0;

            ((RectTransform) speechBubble.transform).position = worldToScreenPoint;
        }

        public void Hide(SpeakerComponent speaker)
        {
            if (_speechBubbles.TryGetValue(speaker, out SpeechBubble speechBubble))
            {
                GameObject.Destroy(speechBubble.gameObject);
                _speechBubbles.Remove(speaker);
            }
        }
    }
}