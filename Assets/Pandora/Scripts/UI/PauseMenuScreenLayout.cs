using System.Collections;
using Pandora.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class PauseMenuScreenLayout : MonoBehaviour
    {
        public Button _resumeButton;
        public Button _quitButton;

        private GameManager _gameManager;
            
        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            
            _resumeButton.onClick.AddListener(OnResumeButton);
            _quitButton.onClick.AddListener(OnQuitButton);
        }


        private void OnEnable()
        {
            StartCoroutine(ActivateButton(_resumeButton));     
        }

        private IEnumerator ActivateButton(Button playButton)
        {
            yield return null;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(playButton.gameObject);
        }


        private void OnResumeButton()
        {
            _gameManager.SendMessage(Message.ON_CANCEL_BUTTON);
        }

        private void OnQuitButton()
        {
            _gameManager.SendMessage(Message.ON_START_BUTTON);
        }
    }
}