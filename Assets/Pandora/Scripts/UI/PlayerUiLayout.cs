using UnityEngine;

namespace Pandora.UI
{
    public class PlayerUiLayout : MonoBehaviour
    {
        public ProgressBarLayout _strongAttackCooldownIcon;
        public ProgressBarLayout _stunAttackCooldownIcon;
        public ProgressBarLayout _tauntCooldownIcon;
    }
}