using System;
using System.Collections.Generic;
using Pandora.Components;
using Pandora.GameState;
using Pandora.Managers;
using UnityEngine;

namespace Pandora.UI
{
    public class LifeBarScreenLayout : MonoBehaviour
    {
        public ProgressBarLayout _lifeBarPrefab;
        public RectTransform _lifeBarsLayer;

        private readonly Dictionary<AttributesComponent, ProgressBarLayout> _progressBarLayouts = new Dictionary<AttributesComponent, ProgressBarLayout>();
        private LevelManager _levelManager;
        private GameManager _gameManager;

        private void Awake()
        {
            _levelManager = FindObjectOfType<LevelManager>();
            _gameManager = FindObjectOfType<GameManager>();
        }

        public void SetLifeBar(AttributesComponent attributesComponent)
        {
            if (!_progressBarLayouts.ContainsKey(attributesComponent))
            {
                _progressBarLayouts.Add(attributesComponent, GameObject.Instantiate(_lifeBarPrefab, _lifeBarsLayer));
            }

            Vector3 worldToScreenPoint = _levelManager._ingameCamera.WorldToScreenPoint(attributesComponent.transform.position + Vector3.up * 3);

            worldToScreenPoint.z = 0;

            ProgressBarLayout progressBarLayout = _progressBarLayouts[attributesComponent];
            
            ((RectTransform) progressBarLayout.transform).position = worldToScreenPoint;
            progressBarLayout.SetProgress(attributesComponent._health, attributesComponent._maxHealth);
        }

        private void Update()
        {
            bool doesShowLifeBars = true;

            if (_gameManager != null)
            {
                doesShowLifeBars = _gameManager.IsCurrentState<IngameGameState>();
            }
            
            foreach (KeyValuePair<AttributesComponent,ProgressBarLayout> progressBarLayout in _progressBarLayouts)
            {
                progressBarLayout.Value.gameObject.SetActive(doesShowLifeBars && (progressBarLayout.Key.GetState() != AttributesComponent.CharacterState.Dead));
            }
        }

        public void HideLifeBar(AttributesComponent attributesComponent)
        {
            if (_progressBarLayouts.TryGetValue(attributesComponent, out ProgressBarLayout progressBarLayout))
            {
                progressBarLayout.gameObject.SetActive(false);
            }
        }
    }
}