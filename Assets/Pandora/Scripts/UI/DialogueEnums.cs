using System;

namespace Pandora.UI
{
    [Serializable]
    public enum Character
    {
        PLAYER,
        HELPLESS,
        ENEMY
    }

    [Serializable]
    public enum Expression
    {
        NEUTRAL,
        ANGRY,
        HAPPY,
        SAD
    }
}
