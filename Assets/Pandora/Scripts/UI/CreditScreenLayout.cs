using System;
using System.Collections;
using Pandora.Managers;
using TMPro;
using UnityEngine;

namespace Pandora.UI
{
    public class CreditScreenLayout : MonoBehaviour
    {
        private static readonly float BLINK_SPEED = 1.0f;
        
        public TMP_Text _text;
        
        private GameManager _gameManager;
        private float _blinkValue;

        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
        }

        private void Update()
        {
            if (_text != null)
            {
                if (_text.alpha >= 1)
                {
                    _blinkValue = -BLINK_SPEED;
                }
                else if (_text.alpha <= 0)
                {
                    _blinkValue = BLINK_SPEED;
                }

                _text.alpha += _blinkValue * Time.deltaTime;
            }

            if (_gameManager._playerInput.actions["UI/Cancel"].ReadValue<float>() > 0)
            {
                OnNextButton();
            }
        }

        private void OnNextButton()
        {
            _gameManager.SendMessage(Message.ON_CANCEL_BUTTON);
        }
    }
}