using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Pandora.UI
{
    public class SpeechBubble : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        public void Say(string text)
        {
            _text.text = text;
        }
    }
}
