using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class CharacterPortrait : MonoBehaviour
    {
        [Serializable]
        public class Portrait : Pair<Expression, Sprite>
        {
            public Portrait(Expression parFirst, Sprite parSecond) : base(parFirst, parSecond)
            {
            }
        }

        public Character _id;
        
        public List<Portrait> _sprites;

        public Image _portrait;

        private void Awake()
        {
            Debug.Assert(GetSpriteByKey(Expression.NEUTRAL) != null, "All portraits must at least have a NEUTRAL expression.");
            SetExpression(Expression.NEUTRAL);
        }

        public void SetExpression(Expression expression)
        {
            Sprite sprite = GetSpriteByKey(expression);

            if (sprite == null)
            {
                sprite = GetSpriteByKey(Expression.NEUTRAL);
            }

            _portrait.sprite = sprite;
        }

        private Sprite GetSpriteByKey(Expression key)
        {
            foreach (Portrait pair in _sprites)
            {
                if (pair.first == key)
                {
                    return pair.second;
                }
            }

            return null;
        }
    }
}
