using System.Collections;
using Pandora.Components;
using Pandora.Controllers;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class OrderHelplessToMove : ScriptAction
    {
        [SerializeField] private Transform _targetPosition;
        [SerializeField] private bool _waitForTargetReached = false;
        
        public override IEnumerator Execute()
        {
            MovementComponent companionMovement = GameObject.FindObjectOfType<CompanionController>().GetComponent<MovementComponent>();

            companionMovement.MoveTo(_targetPosition.position, true);

            yield return null;

            if (_waitForTargetReached)
            {
                yield return new WaitWhile(companionMovement.IsMoving);
            }
            else
            {
                yield return null;
            }
        }

        public override bool CanExecute()
        {
            MovementComponent companionMovement = GameObject.FindObjectOfType<CompanionController>().GetComponent<MovementComponent>();

            return !companionMovement.IsMoving();
        }
    }
}
