using System;
using System.Collections;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class WaitForSeconds : ScriptAction
    {
        [SerializeField] private float _timeToWait = 1.0f;

        public override IEnumerator Execute()
        {
            yield return new UnityEngine.WaitForSeconds(_timeToWait);
        }
    }
}
