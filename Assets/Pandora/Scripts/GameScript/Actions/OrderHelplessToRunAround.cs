using System.Collections;
using Pandora.Components;
using Pandora.Controllers;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class OrderHelplessToRunAround : ScriptAction
    {
        [SerializeField] private bool _enableRunAround = true;
        
        public override IEnumerator Execute()
        {
            MovementComponent companionMovement = GameObject.FindObjectOfType<CompanionController>().GetComponent<MovementComponent>();

            if (_enableRunAround)
            {
                companionMovement.StartRunningAround();
            }
            else
            {
                companionMovement.StopRunningAround();
            }

            yield return null;
        }
    }
}
