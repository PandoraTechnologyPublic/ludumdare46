using System.Collections;
using Pandora.Managers;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class GameOver : ScriptAction
    {
        [SerializeField] private bool _isWin = true;

        public override IEnumerator Execute()
        {
            LevelManager levelManager = GameObject.FindObjectOfType<LevelManager>();
            levelManager.GameOver(_isWin);

            yield return null;
        }
    }
}
