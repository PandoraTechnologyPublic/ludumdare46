using System;
using System.Collections;
using Pandora.UI;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class SayLine : ScriptAction
    {
        [SerializeField] private Character speaker;
        [SerializeField] private Expression expression = Expression.NEUTRAL;
        [SerializeField] private float duration = 2.5f;
        [SerializeField] private string message;

        public override IEnumerator Execute()
        {
            DialogueUi ui = GameObject.FindObjectOfType<DialogueUi>();

            ui.Say(message, speaker, expression);

            yield return new UnityEngine.WaitForSeconds(duration);

            ui.Hide();
        }
    }
}
