using System.Collections;
using Pandora.Components;
using Pandora.UI;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class BubbleSpeech : ScriptAction
    {
        [SerializeField] private string message;
        [SerializeField] private float duration;
        [SerializeField] private SpeakerComponent _speaker;

        private Coroutine _hideCoroutine;

        public override IEnumerator Execute()
        {
            _speaker.Say(message);

            if (_hideCoroutine != null)
            {
                StopCoroutine(_hideCoroutine);
            }

            _hideCoroutine = StartCoroutine(WaitForDurationThenHide());
            yield return null;
        }

        private IEnumerator WaitForDurationThenHide()
        {
            yield return new UnityEngine.WaitForSeconds(duration);
            _speaker.HideBubble();
            _hideCoroutine = null;
        }
    }
}
