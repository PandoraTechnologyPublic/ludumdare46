using System;
using System.Collections;
using System.Collections.Generic;
using Pandora.Controllers;
using Pandora.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Pandora.GameScript.Actions
{
    public class EnemySurpriseBattle : ScriptAction
    {
        [SerializeField] private Controller _target;
        [SerializeField] private int _amount = 3;
        [SerializeField] private EnemyController _enemyControllerPrefab;
        [SerializeField] private BoxCollider _spawnRegion;
        [SerializeField] private List<BoxCollider> _spawnRegions;
        [SerializeField] private bool _waitForAllToBeKilled = true;

        private int _remainingEnemies = 0;
        
        public override IEnumerator Execute()
        {
            Transform targetTransform;
            
            if (_target != null)
            {
                targetTransform = _target.transform;
            }
            else
            {
                targetTransform = GameObject.FindObjectOfType<CompanionController>().transform;
            }

            _remainingEnemies = 0;
            
            if(_spawnRegion != null)
            {
                SpawnEnemies(_spawnRegion, _amount, _target);
            }

            foreach (BoxCollider spawnRegion in _spawnRegions)
            {
                SpawnEnemies(spawnRegion, _amount, _target);
            }

            if (_waitForAllToBeKilled)
            {
                yield return new WaitUntil(() => _remainingEnemies <= 0);
            }
            else
            {
                yield return null;
            }
        }

        private void SpawnEnemies(BoxCollider spawnRegion, int enemyCount, Controller target)
        {
            for (int enemyIndex = 0; enemyIndex < enemyCount; ++enemyIndex)
            {
                Vector3 position = spawnRegion.transform.TransformPoint(spawnRegion.center);
                Vector3 spawnRegionSize = spawnRegion.size;
                position.x += Random.Range(-spawnRegionSize.x / 2, spawnRegionSize.x / 2);
                position.z += Random.Range(-spawnRegionSize.y / 2, spawnRegionSize.y / 2);
                position.y -= spawnRegionSize.y / 2;

                EnemyController enemyController = GameObject.Instantiate(_enemyControllerPrefab, position, Quaternion.identity);

                if (_target != null)
                {
                    enemyController.SetDefaultTarget(target);
                }

                enemyController.transform.LookAt(target.transform);
                enemyController.OnEnemyDeath += _ =>
                {
                    --_remainingEnemies;
                };

                ++_remainingEnemies;
            }
        }
    }
}
