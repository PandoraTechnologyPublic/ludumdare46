using System;
using System.Collections;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class SetTriggerEnabled : ScriptAction
    {
        [SerializeField] private bool _enable = true;
        [SerializeField] private TriggerComponent _trigger;

        public override IEnumerator Execute()
        {
            _trigger.SetTriggerEnabled(_enable);
            _trigger.gameObject.SetActive(_enable);
            yield return null;
        }
    }
}
