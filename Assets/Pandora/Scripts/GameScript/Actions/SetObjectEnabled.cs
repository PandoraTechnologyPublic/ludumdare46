using System;
using System.Collections;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class SetObjectEnabled : ScriptAction
    {
        [SerializeField] private bool _enable = true;
        [SerializeField] private GameObject _target;
        //[SerializeField] private string _animationTriggerName;

        public override IEnumerator Execute()
        {
            _target.SetActive(_enable);
            yield return null;
        }
    }
}
