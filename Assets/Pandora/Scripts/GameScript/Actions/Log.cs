using System;
using System.Collections;
using UnityEngine;

namespace Pandora.GameScript.Actions
{
    public class Log : ScriptAction
    {
        [SerializeField] private string message;

        public override IEnumerator Execute()
        {
            Debug.Log("LOG ACTION: " + message);
            yield return null;
        }
    }
}
