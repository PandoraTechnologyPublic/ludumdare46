using System;
using System.Collections;
using System.Collections.Generic;
using Pandora.Managers;
using UnityEngine;

namespace Pandora.GameScript
{
    public class TriggerComponent : MonoBehaviour
    {
        [Serializable]
        public enum EnabledTargets
        {
            HELPLESS,
            PLAYER,
            BOTH
        }

        [SerializeField] private EnabledTargets _enabledTargets = EnabledTargets.HELPLESS;
        [SerializeField] private bool _enabled = true;

        public void SetTriggerEnabled(bool enabled)
        {
            _enabled = enabled;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_enabled || 
                (_enabledTargets == EnabledTargets.HELPLESS && !other.CompareTag("helpless")) ||
                (_enabledTargets == EnabledTargets.PLAYER && !other.CompareTag("Player")) ||
                (_enabledTargets == EnabledTargets.BOTH && !other.CompareTag("helpless") && !other.CompareTag("Player")))
            {
                return;
            }

            _enabled = false; // to prevent re-triggering

            List<ScriptAction> actions = new List<ScriptAction>();
            int nChildren = transform.childCount;
            for (int i = 0; i < nChildren; ++i)
            {
                Transform child = transform.GetChild(i);
                if (child.gameObject.activeSelf)
                {
                    ScriptAction action = transform.GetChild(i).GetComponent<ScriptAction>();
                    if (action.enabled)
                    {
                        actions.Add(action);
                    }
                }
            }
            
            LevelManager levelManager = GameObject.FindObjectOfType<LevelManager>();
            levelManager.PlayScript(actions);
        }
    }
}
