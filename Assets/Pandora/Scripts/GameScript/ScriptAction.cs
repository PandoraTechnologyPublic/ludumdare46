using System;
using System.Collections;
using UnityEngine;

namespace Pandora.GameScript
{
    public abstract class ScriptAction : MonoBehaviour
    {
        public abstract IEnumerator Execute();
        //{
        //    Debug.LogError("IMPLEMENT THIS IN SUBCLASS");
        //    yield return null;
        //}

        public virtual bool CanExecute()
        {
            return true;
        }
    }
}
