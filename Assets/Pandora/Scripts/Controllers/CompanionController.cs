using System.Collections;
using Pandora.Components;
using Pandora.Managers;
using Pandora.UI;
using UnityEngine;

namespace Pandora.Controllers
{
    public class CompanionController : Controller
    {
        [SerializeField] private Camera _mainCamera;
        
        private MovementComponent _movementComponent;
        private AttributesComponent _attributesComponent;

        private void Awake()
        {
            _attributesComponent = GetComponent<AttributesComponent>();
            _movementComponent = GetComponent<MovementComponent>();
        }

        private void Update()
        {
            if (_attributesComponent.GetState() == AttributesComponent.CharacterState.Dead)
            {
                enabled = false;
                LevelManager levelManager = GameObject.FindObjectOfType<LevelManager>();
                levelManager.StartCoroutine(CompanionDie());

                //TODO:display some death stuff!
            }
        }

        private IEnumerator CompanionDie()
        {
            _movementComponent.DisableMovement();
            yield return new WaitForSeconds(5.0f);
            LevelManager levelManager = GameObject.FindObjectOfType<LevelManager>();
            levelManager.GameOver(false);
        }
    }
}
