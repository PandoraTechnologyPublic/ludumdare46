﻿using System;
using Pandora.Components;
using Pandora.Managers;
using Pandora.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Pandora.Controllers
{

    [DefaultExecutionOrder(100)]
    public class PlayerController : Controller
    {
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private SpeechBubble _speechBubble;
        [SerializeField] private float _speed;
        [SerializeField] private float _angularSpeed;
        [SerializeField] private float _gravity;
        
        private GameManager _gameManager;
        private PlayerInput _playerInput;
        private Coroutine _attackCoroutine;

        private AttributesComponent _attributesComponent;
        private CharacterController _controller;
        
        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
            _attributesComponent = GetComponent<AttributesComponent>();
            _controller = GetComponent<CharacterController>();

            _gameManager = GameObject.FindObjectOfType<GameManager>();
            
            if (_gameManager != null)
            {
                GameObject.Destroy(_playerInput);
                _playerInput = _gameManager._playerInput;
                _playerInput.SwitchCurrentActionMap("Player");
            }
        }

        private void OnEnable()
        {
            _playerInput.actions["Player/Pause"].performed += OnPause;
        }

        private void OnDisable()
        {
            
            _playerInput.actions["Player/Pause"].performed -= OnPause;
        }

        private void OnPause(InputAction.CallbackContext callbackContext)
        {
            if (_gameManager != null)
            {
                _gameManager.SendMessage(Message.ON_CANCEL_BUTTON);
            }
        }

        private void FixedUpdate()
        {
            Vector3 worldDirection = Vector3.zero;

            if (_attributesComponent.CanMove())
            {
                Vector2 playerMovement = _playerInput.actions["Player/Move"].ReadValue<Vector2>();
                worldDirection = _mainCamera.transform.TransformDirection(playerMovement.x, 0, playerMovement.y);
                worldDirection.y = 0;

                if (worldDirection.sqrMagnitude > 0.01)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(worldDirection), _angularSpeed * Time.fixedDeltaTime);
                }

                worldDirection *= _speed;
            }

            worldDirection.y -= _gravity * Time.fixedDeltaTime; // gravity, to keep it grounded when it goes down the stairs or across ledges
            _controller.Move(worldDirection * Time.fixedDeltaTime);
        }

        public PlayerInput GetCurrentPlayerInput()
        {
            return _playerInput;
        }
    }
}
