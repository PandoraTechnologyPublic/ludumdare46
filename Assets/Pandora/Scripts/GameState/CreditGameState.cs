using System;
using Pandora.Managers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Pandora.GameState
{
    public class CreditGameState : GameState
    {
        public override void OnEnter(GameManager gameManager)
        {
            gameManager._creditScreenLayout.gameObject.SetActive(true);
        }

        public override void OnExit(GameManager gameManager)
        {
            gameManager._creditScreenLayout.gameObject.SetActive(false);
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            switch (message)
            {
                case Message.ON_CANCEL_BUTTON:
                {
                    gameManager.ChangeState<MainMenuGameState>();
                    return true;
                }
            }

            return false;
        }
    }
}