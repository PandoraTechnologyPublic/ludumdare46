using Pandora.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.GameState
{
    public class IngameGameState : GameState
    {
        public override void OnEnter(GameManager gameManager)
        {
            gameManager._ingameScreenLayout.gameObject.SetActive(true);
            
            gameManager._playerInput.SwitchCurrentActionMap("Player");
            Time.timeScale = 1.0f;
        }

        public override void OnExit(GameManager gameManager)
        {
            Time.timeScale = 0.0f;
            
            gameManager._ingameScreenLayout.gameObject.SetActive(false);
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            switch (message)
            {
                case Message.ON_CANCEL_BUTTON:
                {
                    gameManager.ChangeState<PauseMenuGameState>();
                    return true;
                }
            }

            return false;
        }
    }
}