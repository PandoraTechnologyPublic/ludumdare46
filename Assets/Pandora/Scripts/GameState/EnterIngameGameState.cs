using Pandora.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.GameState
{
    public class EnterIngameGameState : GameState
    {
        public override void OnEnter(GameManager gameManager)
        {
            gameManager._menuCamera.GetComponent<AudioListener>().enabled = false;
            SceneManager.UnloadSceneAsync(Config.MENU_DECOR_SCENE_NAME);
            SceneManager.LoadScene(Config.LEVEL_SCENE_NAME, LoadSceneMode.Additive);
            
            gameManager.ChangeState<IngameGameState>();
        }

        public override void OnExit(GameManager gameManager)
        {
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            return false;
        }
    }
}