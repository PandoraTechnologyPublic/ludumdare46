using Pandora.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.GameState
{
    public class ExitIngameGameState : GameState
    {
        public override void OnEnter(GameManager gameManager)
        {
            gameManager.ChangeState<GameOverGameState>();
        }

        public override void OnExit(GameManager gameManager)
        {
            gameManager._playerInput.SwitchCurrentActionMap("UI");
            Camera.main.GetComponent<AudioListener>().enabled = false;
            
            SceneManager.UnloadSceneAsync(Config.LEVEL_SCENE_NAME);
            gameManager._menuCamera.GetComponent<AudioListener>().enabled = true;
            SceneManager.LoadScene(Config.MENU_DECOR_SCENE_NAME, LoadSceneMode.Additive);
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            return false;
        }
    }
}