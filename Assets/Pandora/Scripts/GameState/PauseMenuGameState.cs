using Pandora.Managers;
using Pandora.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.GameState
{
    public class PauseMenuGameState : GameState
    {
        private DialogueUi _dialogueUi;
        private LifeBarScreenLayout _lifeBarScreenLayout;

        public override void OnEnter(GameManager gameManager)
        {
            gameManager._playerInput.SwitchCurrentActionMap("UI");
            gameManager._pauseMenuScreenLayout.gameObject.SetActive(true);
            
            _dialogueUi = GameObject.FindObjectOfType<DialogueUi>();
            _dialogueUi.gameObject.SetActive(false);
            
            _lifeBarScreenLayout = GameObject.FindObjectOfType<LifeBarScreenLayout>();
            _lifeBarScreenLayout.gameObject.SetActive(false);
        }

        public override void OnExit(GameManager gameManager)
        {
            gameManager._pauseMenuScreenLayout.gameObject.SetActive(false);
            _dialogueUi.gameObject.SetActive(true);
            _lifeBarScreenLayout.gameObject.SetActive(true);
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            switch (message)
            {
                case Message.ON_START_BUTTON:
                {
                    gameManager.ChangeState<ExitIngameGameState>();
                    return true;
                }
                case Message.ON_CANCEL_BUTTON:
                {
                    gameManager.ChangeState<IngameGameState>();
                    return true;
                }
            }

            return false;
        }
    }
}