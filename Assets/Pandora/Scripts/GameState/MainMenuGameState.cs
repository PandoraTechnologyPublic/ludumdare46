using System;
using Pandora.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Pandora.GameState
{
    public class MainMenuGameState : GameState
    {
        public override void OnEnter(GameManager gameManager)
        {
            gameManager._mainMenuScreenLayout.gameObject.SetActive(true);
        }

        public override void OnExit(GameManager gameManager)
        {
            gameManager._mainMenuScreenLayout.gameObject.SetActive(false);
        }

        public override void OnUpdate(GameManager gameManager)
        {
            
        }

        public override bool HandleMessage(GameManager gameManager, Message message)
        {
            switch (message)
            {
                case Message.ON_START_BUTTON:
                {
                    gameManager.ChangeState<EnterIngameGameState>();
                    return true;
                }
                case Message.ON_CREDIT_BUTTON:
                {
                    gameManager.ChangeState<CreditGameState>();
                    return true;
                }
            }

            return false;
        }
    }
}