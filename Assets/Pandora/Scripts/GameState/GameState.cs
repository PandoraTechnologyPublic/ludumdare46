using Pandora.Managers;

namespace Pandora.GameState
{
    public abstract class GameState
    {
        public abstract void OnEnter(GameManager gameManager);
        public abstract void OnExit(GameManager gameManager);
        public abstract void OnUpdate(GameManager gameManager);

        public abstract bool HandleMessage(GameManager gameManager, Message message);
    }
}