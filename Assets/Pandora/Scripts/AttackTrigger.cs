﻿using System.Collections.Generic;
using Pandora.Components;
using UnityEngine;

namespace Pandora
{
    public class AttackTrigger : MonoBehaviour
    {
        public List<AttackComponent> _attackComponents;
    
        private void OnTriggerEnter(Collider other)
        {
            foreach (AttackComponent component in _attackComponents)
            {
                component.AttackTriggerHit(other);
            }
        }
    }
}
