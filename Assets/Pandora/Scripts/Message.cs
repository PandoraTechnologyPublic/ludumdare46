namespace Pandora
{
    public enum Message
    {
        ON_START_BUTTON,
        ON_CANCEL_BUTTON,
        ON_CREDIT_BUTTON,
    }
}