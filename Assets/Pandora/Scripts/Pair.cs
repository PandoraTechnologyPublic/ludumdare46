﻿using System;

namespace Pandora
{
    [Serializable]
    public class Pair<TYPE_FIRST, TYPE_SECOND>
    {
        public TYPE_FIRST first;
        public TYPE_SECOND second;

        public Pair(TYPE_FIRST parFirst, TYPE_SECOND parSecond)
        {
            first = parFirst;
            second = parSecond;
        }

        public override string ToString()
        {
            return string.Format("First: {0}, Second: {1}", first, second);
        }
    }
}
