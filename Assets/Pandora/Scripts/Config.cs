namespace Pandora
{
    public static class Config
    {
        public static readonly float PLAYER_SPEED = 100.0f;
        public static readonly string LEVEL_SCENE_NAME = "Level Design";
        public static readonly string MENU_DECOR_SCENE_NAME = "Menu3dEnv";
    }
}