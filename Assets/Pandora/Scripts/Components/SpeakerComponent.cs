using Pandora.UI;
using UnityEngine;

namespace Pandora.Components
{
    public class SpeakerComponent : MonoBehaviour
    {
        private DialogBubbleScreenLayout _dialogBubbleScreenLayout;

        private void Awake()
        {
            _dialogBubbleScreenLayout = GameObject.FindObjectOfType<DialogBubbleScreenLayout>();
        }
        
        public void Say(string text)
        {
            _dialogBubbleScreenLayout.Say(this, text);
        }

        public void HideBubble()
        {
            _dialogBubbleScreenLayout.Hide(this);
        }
    }
}