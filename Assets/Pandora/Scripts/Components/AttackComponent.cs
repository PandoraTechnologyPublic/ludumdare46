﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pandora.Controllers;
using Pandora.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Pandora.Components
{
    [DefaultExecutionOrder(200)]
    public class AttackComponent : MonoBehaviour
    {
        public enum AttackType
        {
            Damage,
            Stun,
            Taunt
        }

        [Serializable]
        public class AttackEffects
        {
            public float attackValue;
            public AttackType attackType;
        }

        public bool _isPlayable = false;
        
        [Header("Key binding settings")]
        public string _keyBindingName;
        
        [Header("Attack attributes")]
        [Tooltip("This attack won't be usable for that many seconds")]
        public float _attackCooldown = 3.0f;
        public AttackType _soundAttackType = AttackType.Damage;

        [Tooltip("No attacks will be usable for that many seconds, but controller will still be able to move")]
        public float _attackDuration = 1.0f;

        [Tooltip("No attacks nor movement will be usable for that many seconds")]
        public float _castTime = 0.0f;

        public List<AttackEffects> _attackEffects;
        public List<GameObject> _graphicalEffects;
        public SoundComponent _soundComponent;
        public Collider _attackCollider;

        public ProgressBarLayout _cooldownIcon;

        private List<Collider> _targetList;
        private AttributesComponent _attributesComponent;
        private float _cooldown = 0;
        private bool _attacking = false;
        private AnimationComponent _animationComponent;

        private Animator _attackAnimator;
        private static readonly int ATTACK = Animator.StringToHash("attack");
        private PlayerInput _playerInput;

        public void Awake()
        {
            _targetList = new List<Collider>();
            _attributesComponent = GetComponentInParent<AttributesComponent>();
            _animationComponent = GetComponentInParent<AnimationComponent>();

            _attackAnimator = GetComponent<Animator>();
            
            foreach (GameObject attackEffect in _graphicalEffects)
            {
                attackEffect.SetActive(false);
            }

            PlayerController pc = GetComponentInParent<PlayerController>();
            if (pc != null)
            {
                _playerInput = GetComponentInParent<PlayerController>().GetCurrentPlayerInput();
            }
            else if (_isPlayable)
            {
                Debug.LogWarning("Can't have playable character without PlayerController attached on it");
                _isPlayable = false;
            }
        }
        
        public void OnEnable()
        {
            _attackCollider.gameObject.SetActive(false);
            
            if (_isPlayable)
            {
                _playerInput.actions[_keyBindingName].performed += OnAttack;
            }
        }

        private void OnDisable()
        {
            if (_isPlayable)
            {
                if (_playerInput != null)
                {
                    _playerInput.actions[_keyBindingName].performed -= OnAttack;
                }
            }
        }

        public void Attack()
        {
            OnAttack(new InputAction.CallbackContext());
        }

        private void OnAttack(InputAction.CallbackContext callbackContext)
        {
            if (_attributesComponent.CanAttack() && _cooldown <= 0)
            {
                if (_castTime > 0)
                {
                    _attributesComponent.StartCastTime(_castTime);
                }

                _attacking = true;
                _cooldown = _attackCooldown;
                _attributesComponent.SetAttackCooldown(_attackDuration);
                _attackCollider.gameObject.SetActive(true);
            
                foreach (GameObject attackEffect in _graphicalEffects)
                {
                    attackEffect.SetActive(false);
                    attackEffect.SetActive(true);
                }
                
                foreach (AttackEffects attackEffect in _attackEffects)
                {
                    _animationComponent.PlayAttackAnimation(attackEffect.attackType);
                }

                if (_soundComponent != null)
                {
                    _soundComponent.PlayAttackSound(_soundAttackType);
                }

                if (_attackAnimator != null)
                {
                    _attackAnimator.SetTrigger(ATTACK);
                }

                StartCoroutine(AttackCoroutine());
            }
        }

        private IEnumerator AttackCoroutine()
        {
            yield return new WaitForSeconds(_attackDuration);
            _attacking = false;
            _attackCollider.gameObject.SetActive(false);
            _targetList.Clear();
            yield return null;
        }

        public void AttackTriggerHit(Collider target)
        {
            if (_attacking && target.gameObject.layer == 11 && !_targetList.Contains(target))
            {
                _targetList.Add(target);
                
                AttributesComponent targetAttributesComponent = target.GetComponentInParent<AttributesComponent>();
                if (targetAttributesComponent != null)
                {
                    if (targetAttributesComponent._teamId != _attributesComponent._teamId && targetAttributesComponent.GetState() != AttributesComponent.CharacterState.Dead)
                    {
                        DealAttackEffects(targetAttributesComponent);
                    }
                }
            }
        }

        public void DealAttackEffects(AttributesComponent targetAttributesComponent)
        {
            foreach (AttackEffects attackEffect in _attackEffects)
            {
                switch (attackEffect.attackType)
                {
                    case AttackType.Damage:
                    {
                        targetAttributesComponent.Hit(attackEffect.attackValue);
                        break;
                    }
                    case AttackType.Stun:
                    {
                        targetAttributesComponent.StartCoroutine(targetAttributesComponent.Stun(attackEffect.attackValue));
                        break;
                    }
                    case AttackType.Taunt:
                    {
                        if (targetAttributesComponent.GetState() != AttributesComponent.CharacterState.Stunned && targetAttributesComponent.TryGetComponent<EnemyController>(out EnemyController enemyComponent))
                        {
                            enemyComponent._playerAggro += attackEffect.attackValue;
                        }
                        
                        break;
                    }
                }
            }
        }

        private void FixedUpdate()
        {
            _cooldown = Math.Max(0, _cooldown - Time.fixedDeltaTime);
            
            if (_cooldownIcon != null)
            {
                //_cooldownIcon.gameObject.SetActive(_cooldown > 0);
                _cooldownIcon.SetProgress(_cooldown, _attackCooldown);
            }
        }
    }
}