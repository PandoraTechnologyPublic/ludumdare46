﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Pandora.Components
{
    [DefaultExecutionOrder(50)]
    public class MovementComponent : MonoBehaviour
    {
        [SerializeField] private float _runningAroundRadius = 15;
        [SerializeField] private float _runningSpeedMult = 2.0f;
        [SerializeField] private float _runningAccelerationMult = 1.5f;
        [SerializeField] private float _gravity = 800;
        [SerializeField] private bool _startRunningAround = false;

        private NavMeshAgent _navMeshAgent;
        private CharacterController _charControl;
        private Coroutine _runningAroundCoroutine;
        private Vector3 _runningAroundCenter;

        private Vector3? _stickyDestination = null;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _charControl = GetComponent<CharacterController>();

            _navMeshAgent.updatePosition = false;
            _runningAroundRadius = Mathf.Max(1, _runningAroundRadius);
        }

        private void OnEnable()
        {
            if (_startRunningAround)
            {
                StartRunningAround(1);
            }
        }

        private void OnDisable()
        {
            StopRunningAround();
        }

        public void Update()
        {
            if (_stickyDestination != null)
            {
                _navMeshAgent.SetDestination(_stickyDestination.Value);
            }
        }

        public void FixedUpdate()
        {
            Vector3 desVelocity = new Vector3(0, 0, 0);

            if (_navMeshAgent.enabled && _navMeshAgent.hasPath)
            {
                desVelocity = _navMeshAgent.desiredVelocity;
            }

            desVelocity.y = -_gravity * Time.fixedDeltaTime;
            _charControl.Move(desVelocity * Time.fixedDeltaTime);
            _navMeshAgent.velocity = _charControl.velocity;
        }

        public void MoveTo(Vector3 position, bool isStickyDestination = false)
        {
            if (isStickyDestination)
            {
                _stickyDestination = position;
            }
            else
            {
                _stickyDestination = null;
            }

            if (_navMeshAgent.enabled)
            {
                _navMeshAgent.SetDestination(position);
            }
        }

        public bool IsMoving()
        {
            return _navMeshAgent.hasPath;
        }

        public void StartRunningAround(float delay = -1)
        {
            if (_runningAroundCoroutine != null)
            {
                return;
            }

            _runningAroundCenter = transform.position;
            _runningAroundCenter.y = 0;
            _runningAroundCoroutine = StartCoroutine(RunAround(delay));
        }

        private IEnumerator RunAround(float delay)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            _navMeshAgent.autoBraking = false;
            _navMeshAgent.speed *= _runningSpeedMult;
            _navMeshAgent.acceleration *= _runningAccelerationMult;

            Vector2 minOffset = new Vector2(0.2f, 0.2f);
            
            while (true)
            {
                int i = 100; // to not loop too many times, just in case
                Vector3 dest = Vector3.zero;
                while (i-- > 0)
                {
                    bool canGo;
                    dest = _runningAroundCenter + Random.insideUnitSphere * _runningAroundRadius;
                    dest.y = _runningAroundCenter.y;
                    canGo = NavMesh.SamplePosition(dest, out NavMeshHit hit, Mathf.Max(_runningAroundRadius, 15), NavMesh.AllAreas);

                    if (canGo && hit.distance > 1 && hit.distance < _runningAroundRadius)
                    {
                        dest = hit.position;
                        break;
                    }
                }

                MoveTo(dest);

                // Go to "dest" until reached OR a max amount of time passed
                float untilMaxTime = Time.time + Random.Range(1.5f, 2.5f);

                while (!IsMoving())
                {
                    yield return null;
                }

                while (!(Time.time >= untilMaxTime || !IsMoving() || (transform.position - dest).magnitude <= 0.5f))
                {
                    yield return null;
                }
            }
        }

        public void StopRunningAround()
        {
            if (_runningAroundCoroutine != null)
            {
                _navMeshAgent.autoBraking = true;
                _navMeshAgent.speed /= _runningSpeedMult;
                _navMeshAgent.acceleration /= _runningAccelerationMult;
                StopCoroutine(_runningAroundCoroutine);
                _runningAroundCoroutine = null;
                _navMeshAgent.enabled = false;
                _navMeshAgent.enabled = true;
            }
        }

        public void DisableMovement()
        {
            _navMeshAgent.enabled = false;
            enabled = false;
        }
    }
}
