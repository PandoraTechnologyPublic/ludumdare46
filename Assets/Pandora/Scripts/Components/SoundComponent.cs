using UnityEngine;

namespace Pandora.Components
{
    public class SoundComponent : MonoBehaviour
    {
        [SerializeField] private AudioSource _attackSound;
        [SerializeField] private AudioSource _hitSound;
        [SerializeField] private AudioSource _deathSound;
        [SerializeField] private AudioSource _tauntSound;

        private void Awake()
        {
            if (_attackSound != null)
            {
                _attackSound.gameObject.SetActive(false);
            }

            if (_hitSound != null)
            {
                _hitSound.gameObject.SetActive(false);
            }

            if (_deathSound != null)
            {
                _deathSound.gameObject.SetActive(false);
            }

            if (_tauntSound != null)
            {
                _tauntSound.gameObject.SetActive(false);
            }
        }

        public void PlayAttackSound(AttackComponent.AttackType soundAttackType)
        {
            if (soundAttackType == AttackComponent.AttackType.Taunt)
            {
                PlayTauntSound();
            }
            else
            {
                if (_attackSound != null)
                {
                    _attackSound.gameObject.SetActive(false);
                    _attackSound.gameObject.SetActive(true);
                }
            }
        }

        public void PlayHitSound()
        {
            if (_hitSound != null)
            {
                _hitSound.gameObject.SetActive(false);
                _hitSound.gameObject.SetActive(true);
            }
        }

        public void PlayDeathSound()
        {
            if (_deathSound != null)
            {
                _deathSound.gameObject.SetActive(false);
                _deathSound.gameObject.SetActive(true);
            }
        }

        public void PlayTauntSound()
        {
            if (_tauntSound != null)
            {
                _tauntSound.gameObject.SetActive(false);
                _tauntSound.gameObject.SetActive(true);
            }
        }
    }
}