﻿using System;
using System.Collections;
using Pandora.UI;
using UnityEngine;

namespace Pandora.Components
{
    public class AttributesComponent : MonoBehaviour
    {
        public enum CharacterState
        {
            Normal,
            Hurt,
            Stunned,
            Dead,
        }
    
        public int _teamId = 1;
        public float _maxHealth = 100.0f;
        public float _health = 100.0f;
        [SerializeField] private float _hurtLimit = 40.0f;
        [SerializeField] private CharacterState _state = CharacterState.Normal;
        private LifeBarScreenLayout _lifeBarScreenLayout;
        private bool _isStunned = false;
        public float _attackCooldown = 0.0f;
        public Transform _stunnedObject;

        private AnimationComponent _animationComponent;
        private SoundComponent _soundComponent;
        private float _remainingCastTime;

        private void Awake()
        {
            _animationComponent = GetComponent<AnimationComponent>();
            _soundComponent = GetComponent<SoundComponent>();
        
            if (_stunnedObject != null)
            {
                _stunnedObject.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            _lifeBarScreenLayout = GameObject.FindObjectOfType<LifeBarScreenLayout>();
        }
    
        private void FixedUpdate()
        {
            _attackCooldown = Math.Max(0, _attackCooldown - Time.fixedDeltaTime);
        }

        public void Update()
        {
            UpdateCharacterState();

            if (_state != CharacterState.Dead)
            {
                _lifeBarScreenLayout.SetLifeBar(this);

                if (_remainingCastTime > 0)
                {
                    _remainingCastTime -= Time.deltaTime;
                }
            }
        }

        public void SetAttackCooldown(float value)
        {
            _attackCooldown = value;
        }

        public bool CanAttack()
        {
            return _attackCooldown <= 0 && _remainingCastTime <= 0;
        }

        public bool CanMove()
        {
            return _remainingCastTime <= 0 && _state != CharacterState.Dead && _state != CharacterState.Stunned;
        }

        public CharacterState GetState()
        {
            return _state;
        }

        public void StartCastTime(float castTime)
        {
            _remainingCastTime = castTime;
        }

        public void Heal(float healPoints)
        {
            if (_state != CharacterState.Dead)
            {
                _health += healPoints;
            
                if (_health > _maxHealth)
                {
                    _health = _maxHealth;
                }
            }
        }

        public void Hit(float damagePoints)
        {
            Debug.Log("Damage taken by " + name + ": " + damagePoints);
            _health -= damagePoints;

            _animationComponent.PlayHitAnimation();
            _soundComponent.PlayHitSound();
        }

        public void Revive(float healthPoints = 20.0f)
        {
            if (_state == CharacterState.Dead)
            {
                _health = healthPoints;
            }
        }

        public IEnumerator Stun(float stunTime = 4.0f)
        {
            _isStunned = true;
            _animationComponent.SetStunned(true);
        
            if (_stunnedObject != null)
            {
                _stunnedObject.gameObject.SetActive(true);
            }
        
            yield return new WaitForSeconds(stunTime);
            _isStunned = false;
        
            if (_stunnedObject != null)
            {
                _stunnedObject.gameObject.SetActive(false);
            }
            _animationComponent.SetStunned(false);
        
            yield return null;
        }

        private void UpdateCharacterState()
        {
            if (_health <= 0.0f)
            {
                if (_state != CharacterState.Dead)
                {
                    StartCoroutine(Die());
                }
                _state = CharacterState.Dead;
                _health = 0.0f;
            
            }
            else if (_isStunned)
            {
                _state = CharacterState.Stunned;
            }
            else if (_health <= _hurtLimit)
            {
                _state = CharacterState.Hurt;
            }
            else
            {
                _state = CharacterState.Normal;
            }
        }

        private IEnumerator Die()
        {
            _soundComponent.PlayDeathSound();
            _lifeBarScreenLayout.HideLifeBar(this);
            _animationComponent.PlayDeathAnimation();
            yield return new WaitForSeconds(5.0f);
            float until = Time.time + 1;
            while (Time.time < until)
            {
                Vector3 newPos = transform.position;
                newPos.y -= 2f * Time.deltaTime;
                transform.position = newPos;
                yield return null;
            }
            GameObject.Destroy(gameObject);
        }
    }
}
