﻿using System;
using System.Collections.Generic;
using Pandora.GameState;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Pandora.Managers
{
    public class GameManager : MonoBehaviour
    {
        public Canvas _creditScreenLayout;
        public Canvas _mainMenuScreenLayout;
        public Canvas _gameOverScreenLayout;
        public Canvas _ingameScreenLayout;
        public Canvas _pauseMenuScreenLayout;

        public Camera _menuCamera;
        
        private readonly Dictionary<Type, GameState.GameState> _gameStates = new Dictionary<Type, GameState.GameState>();
        private GameState.GameState _currentState;
        public PlayerInput _playerInput;

        private void Awake()
        {
            _creditScreenLayout.gameObject.SetActive(false);
            _mainMenuScreenLayout.gameObject.SetActive(false);
            _gameOverScreenLayout.gameObject.SetActive(false);
            _ingameScreenLayout.gameObject.SetActive(false);
            _pauseMenuScreenLayout.gameObject.SetActive(false);
            
            SceneManager.LoadScene(Config.MENU_DECOR_SCENE_NAME, LoadSceneMode.Additive);
            
            DefineState<MainMenuGameState>();
            DefineState<EnterIngameGameState>();
            DefineState<IngameGameState>();
            DefineState<PauseMenuGameState>();
            DefineState<ExitIngameGameState>();
            DefineState<GameOverGameState>();
            DefineState<CreditGameState>();

            ChangeState<MainMenuGameState>();
        }

        private void Update()
        {
            _currentState?.OnUpdate(this);
        }

        private void DefineState<TYPE_STATE>() where TYPE_STATE : GameState.GameState, new()
        {
            Type type = typeof(TYPE_STATE);
            _gameStates.Add(type, new TYPE_STATE());
        }

        public void ChangeState<TYPE_STATE>()
        {
            Type type = typeof(TYPE_STATE);

            _currentState?.OnExit(this);

            _currentState = _gameStates[type];
            
            _currentState.OnEnter(this);
        }

        public void SendMessage(Message message)
        {
            _currentState.HandleMessage(this, message);
        }

        public bool IsCurrentState<TYPE_STATE>()
        {
            return _currentState == _gameStates[typeof(TYPE_STATE)];
        }
    }
}
