using System.Collections;
using System.Collections.Generic;
using Pandora.GameScript;
using Pandora.GameState;
using UnityEditor;
using UnityEngine;

namespace Pandora.Managers
{
    public class LevelManager : MonoBehaviour
    {
        public Camera _ingameCamera;

        private Coroutine _currentCoroutine;

        public void GameOver(bool isWin)
        {
            if (isWin)
            {
                Debug.Log("You won!");
            }
            else
            {
                Debug.Log("You lose!");
            }

            GameManager gameManager = GameObject.FindObjectOfType<GameManager>();
            
            if (gameManager != null)
            {
                gameManager.ChangeState<ExitIngameGameState>();
            }
            else
            {
                Application.Quit();
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#endif
            }
        }

        public void PlayScript(List<ScriptAction> actions)
        {
            Debug.Assert(_currentCoroutine == null, "Can't have several triggers running at the same time!");
            _currentCoroutine = StartCoroutine(RunActionChain(actions));
        }

        private IEnumerator RunActionChain(List<ScriptAction> actions)
        {
            foreach (ScriptAction action in actions)
            {
                while (!action.CanExecute())
                {
                    yield return null;
                }

                yield return action.Execute();
            }
            
            _currentCoroutine = null;
            yield return null;
        }
    }
}