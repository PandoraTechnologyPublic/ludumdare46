%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SharkLegs
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: SharkyBip
    m_Weight: 1
  - m_Path: SharkyBip/FX_Ctrl01
    m_Weight: 1
  - m_Path: SharkyBip/FX_Ctrl01/FX01
    m_Weight: 1
  - m_Path: SharkyBip/FX_Ctrl02
    m_Weight: 1
  - m_Path: SharkyBip/FX_Ctrl02/FX02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Footsteps
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone/Sharky_Tail01
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone/SharkyBip_Tail002_bone
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone/SharkyBip_Tail002_bone/Sharky_Tail02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone/SharkyBip_Tail002_bone/SharkyBip_Tail003_bone
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/Sharky_Torso1/SharkyBip_Tail001_bone/SharkyBip_Tail002_bone/SharkyBip_Tail003_bone/Sharky_Tail03
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf/Sharky_Calf02
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf/SharkyBip L Foot
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf/SharkyBip L Foot/Sharky_Foot02
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf/SharkyBip L Foot/SharkyBip L Toe0
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyBip
      L Calf/SharkyBip L Foot/SharkyBip L Toe0/SharkyBip L Toe0Nub
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip L Thigh/SharkyThigh02
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/Sharky_Thigh01
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf/Sharky_Calf01
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf/SharkyBip R Foot
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf/SharkyBip R Foot/Sharky_Foot01
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf/SharkyBip R Foot/SharkyBip R Toe0
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip R Thigh/SharkyBip
      R Calf/SharkyBip R Foot/SharkyBip R Toe0/SharkyBip R Toe0Nub
    m_Weight: 0
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/Sharky_Torso2
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/FX_Ctrl03
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/FX_Ctrl03/FX03
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/FX_Ctrl03/FX04
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/Sharky_Eyes
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/Sharky_Fin
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/Sharky_Hammer
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/Sharky_Teeth01
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/SharkyBip_Eyes
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/SharkyBip_Eyes/Sharky_Hammer 1
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/SharkyBip_Jawbone
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/SharkyBip_Jawbone/Sharky_Jaw
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/Sharky_Head/SharkyBip_Jawbone/Sharky_Jaw/Sharky_Teeth02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip Head/SharkyBip HeadNub
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/Sharky_Arm02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm/Sharky_ForeArm02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm/SharkyBip
      L Hand
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm/SharkyBip
      L Hand/Sharky_Hand02
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm/SharkyBip
      L Hand/SharkyBip L Finger0
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip L Clavicle/SharkyBip L UpperArm/SharkyBip L Forearm/SharkyBip
      L Hand/SharkyBip L Finger0/SharkyBip L Finger0Nub
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/Sharky_Arm01
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm/Sharky_ForeArm01
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm/SharkyBip
      R Hand
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm/SharkyBip
      R Hand/Sharky_Hand01
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm/SharkyBip
      R Hand/SharkyBip R Finger0
    m_Weight: 1
  - m_Path: SharkyBip/SharkyBip Pelvis/SharkyBip Spine/SharkyBip Spine1/SharkyBip
      Neck/SharkyBip R Clavicle/SharkyBip R UpperArm/SharkyBip R Forearm/SharkyBip
      R Hand/SharkyBip R Finger0/SharkyBip R Finger0Nub
    m_Weight: 1
